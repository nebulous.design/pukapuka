// ----------------------------------------------------------------------------

// core
const http = require('http')

// local
const app = require('./lib/app.js')
const store = require('./lib/store.js')

// ----------------------------------------------------------------------------
// housekeeping

function cleanup(callback) {
  console.log('Closing server ...')
  server.close(() => {
    console.log('Server Closed')

    console.log('Closing Database ...')
    store.close((err) => {
      if (err) {
        console.warn(err)
        return callback(err)
      }
      console.log('Database Closed')
      callback()
    })
  })
}

// we'll get this from nodemon in development
process.once('SIGUSR2', () => {
  console.log('Received SIGUSR2')
  cleanup((err) => {
    process.kill(process.pid, 'SIGUSR2')
  })
})

process.on('SIGTERM', () => {
  console.log('Received SIGTERM')
  cleanup((err) => {
    console.log('Finished')
    process.exit(err ? 2 : 0)
  })
})

process.on('SIGINT', () => {
  console.log('Received SIGINT')
  cleanup((err) => {
    console.log('Finished')
    process.exit(err ? 2 : 0)
  })
})

// ----------------------------------------------------------------------------
// server

const server = http.createServer()
server.on('request', app)

const port = process.env.PORT || 3000
server.listen(port, () => {
  console.log('Listening on port ' + port)
})

// ----------------------------------------------------------------------------
