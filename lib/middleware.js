// ----------------------------------------------------------------------------

// local
const store = require('./store.js')

// ----------------------------------------------------------------------------

function ensureUser(req, res, next) {
  console.log('middleware.ensureUser()')
  if ( !req.session.user ) {
    res.redirect(302, '/sign-in')
    return
  }

  // store into locals so we can use it in the views
  res.locals.user = req.session.user

  next()
}

function redirectToAdminIfLoggedIn(req, res, next) {
  console.log('middleware.redirectToAdminIfLoggedIn()')
  if ( req.session.user ) {
    res.redirect(302, '/admin/')
    return
  }

  next()
}

function apiCheckUser(req, res, next) {
  if (!req.session.user) {
    res.status(401).json({ status: 401, err: 'Requires Authentication' })
    return
  }

  next()
}

function loadAllBooks(req, res, next) {
  store.allBooks((err, books) => {
    if (err) return next(err)
    res.locals.books = books
    next()
  })
}

function loadAllLocations(req, res, next) {
  store.allLocations((err, locations) => {
    if (err) return next(err)
    res.locals.locations = locations
    next()
  })
}

// ----------------------------------------------------------------------------

module.exports = {
  ensureUser,
  redirectToAdminIfLoggedIn,
  apiCheckUser,

  // store
  loadAllBooks,
  loadAllLocations,
}

// ----------------------------------------------------------------------------
