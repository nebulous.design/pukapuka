// ----------------------------------------------------------------------------

// npm
const sqlite3 = require('sqlite3').verbose()
const lodashWords = require('lodash.words')
const stemr = require('stemr')
const stopword = require('stopword')

// ----------------------------------------------------------------------------
// setup

// now - to get back `2018-06-30T11:04:11.290Z`
const now = "strftime('%Y-%m-%dT%H:%M:%fZ')"

// From : https://www.sqlite.org/c3ref/c_abort.html
const SQLITE_ERROR      =  1   /* Generic error */
const SQLITE_INTERNAL   =  2   /* Internal logic error in SQLite */
const SQLITE_PERM       =  3   /* Access permission denied */
const SQLITE_ABORT      =  4   /* Callback routine requested an abort */
const SQLITE_BUSY       =  5   /* The database file is locked */
const SQLITE_LOCKED     =  6   /* A table in the database is locked */
const SQLITE_NOMEM      =  7   /* A malloc() failed */
const SQLITE_READONLY   =  8   /* Attempt to write a readonly database */
const SQLITE_INTERRUPT  =  9   /* Operation terminated by sqlite3_interrupt()*/
const SQLITE_IOERR      = 10   /* Some kind of disk I/O error occurred */
const SQLITE_CORRUPT    = 11   /* The database disk image is malformed */
const SQLITE_NOTFOUND   = 12   /* Unknown opcode in sqlite3_file_control() */
const SQLITE_FULL       = 13   /* Insertion failed because database is full */
const SQLITE_CANTOPEN   = 14   /* Unable to open the database file */
const SQLITE_PROTOCOL   = 15   /* Database lock protocol error */
const SQLITE_EMPTY      = 16   /* Internal use only */
const SQLITE_SCHEMA     = 17   /* The database schema changed */
const SQLITE_TOOBIG     = 18   /* String or BLOB exceeds size limit */
const SQLITE_CONSTRAINT = 19   /* Abort due to constraint violation */
const SQLITE_MISMATCH   = 20   /* Data type mismatch */
const SQLITE_MISUSE     = 21   /* Library used incorrectly */
const SQLITE_NOLFS      = 22   /* Uses OS features not supported on host */
const SQLITE_AUTH       = 23   /* Authorization denied */
const SQLITE_FORMAT     = 24   /* Not used */
const SQLITE_RANGE      = 25   /* 2nd parameter to sqlite3_bind out of range */
const SQLITE_NOTADB     = 26   /* File opened that is not a database file */
const SQLITE_NOTICE     = 27   /* Notifications from sqlite3_log() */
const SQLITE_WARNING    = 28   /* Warnings from sqlite3_log() */
const SQLITE_ROW        = 100  /* sqlite3_step() has another row ready */
const SQLITE_DONE       = 101  /* sqlite3_step() has finished executing */

// open DB
const dbFilename = '.data/sqlite.db'
const db = new sqlite3.Database(dbFilename)

const patches = [
  // patch 0 -> 1
  {
    forward : `
      CREATE TABLE book (
        isbn TEXT NOT NULL,
        title TEXT NOT NULL,
        author TEXT NOT NULL,
        location TEXT,
        image TEXT,
        tags TEXT,
        note TEXT,

        CONSTRAINT pk PRIMARY KEY (isbn)
      );
    `,
    reverse : 'DROP TABLE book;',
  },
  // patch 1 -> 2
  {
    forward : `
      CREATE TABLE tag (
        isbn TEXT NOT NULL,
        tag TEXT NOT NULL,

        FOREIGN KEY(isbn) REFERENCES book(isbn)
      );
    `,
    reverse : 'DROP TABLE tag;',
  },
  // patch 2 -> 3
  // Rename `tag` table to `stem` (whilst also renaming column `tag` to `stem`)
  {
    forward : [
      `BEGIN TRANSACTION;`,
      `CREATE TABLE stem (
        isbn TEXT NOT NULL,
        stem TEXT NOT NULL,

        FOREIGN KEY(isbn) REFERENCES book(isbn)
      );`,
      `INSERT INTO
        stem(isbn, stem)
      SELECT
        isbn, tag
      FROM
        tag
      ;`,
      `DROP TABLE tag;`,
      `COMMIT;`,
    ],
    reverse : [
      `BEGIN TRANSACTION;`,
      `CREATE TABLE tag (
        isbn TEXT NOT NULL,
        tag TEXT NOT NULL,

        FOREIGN KEY(isbn) REFERENCES book(isbn)
      );`,
      `INSERT INTO
        tag(isbn, tag)
      SELECT
        isbn, stem
      FROM
        stem
      ;`,
      `DROP TABLE stem;`,
      `COMMIT;`,
    ],
  },
  // patch 3 -> 4
  {
    forward : `
      CREATE TABLE tag (
        isbn TEXT NOT NULL,
        tag TEXT NOT NULL,

        FOREIGN KEY(isbn) REFERENCES book(isbn)
      );
    `,
    reverse : `DROP TABLE tag;`,
  },
  // patch 4 -> 5
  {
    forward : `
      CREATE TABLE location (
        id INTEGER PRIMARY KEY,
        title TEXT NOT NULL,
        description TEXT NOT NULL,
        inserted TEXT NOT NULL,
        updated TEXT NOT NULL
      );
    `,
    reverse : `DROP TABLE location;`,
  },
]

// ----------------------------------------------------------------------------

function splitTags(str) {
  return str.split(',').map(t => t.trim().replace(/\s+/g, ' ').toLowerCase()).filter(t => t !== '')
}

function check(err) {
  if (err) console.warn(err)
}

// ----------------------------------------------------------------------------

// runs this particular patch
function do_patch(db, level, from, to, patch) {
  console.log(`Patching ${from} -> ${to} ...`)

  if ( level >= to ) {
    console.log(`Patch level not required : ${to}`)
    return
  }

  // see if we have one patch, or an array, (or dunno)
  if (typeof patch === 'string') {
    db.run(patch, check)
  }
  else if (Array.isArray(patch)) {
    patch.forEach(p => {
      db.run(p, check)
    })
  }
  else {
    // dunno what this is supposed to be
    console.log(`Invalid patch for level ${to}:`, patch)
    process.exit(2)
  }
  db.run(`PRAGMA user_version = ${to}`, check)
}

// runs all patches needed
function patch(db, level, patches) {
  console.log('Doing patches ...')

  do_patch(db, level, 0, 1, patches[0].forward)
  do_patch(db, level, 1, 2, patches[1].forward)
  do_patch(db, level, 2, 3, patches[2].forward)

  // do_patch(db, level, 3, 4, patches[3].forward, ())

  // patch 3 -> 4
  if ( level < 4 ) {
    console.log('Patching 3 -> 4 ...')
    db.run(patches[3].forward, check)

    // update the tags table with the values in `book.tags`

    // get all books
    db.all("SELECT * FROM book", [], (err, books) => {
      console.log('Got books:', err, books)
      if (err) return callback(err)
      books.forEach(book => {
        const tags = splitTags(book.tags)
        tags.forEach(tag => {
          console.log('Inserting ' + tag)
          db.run(sqlInsTag, [ book.isbn, tag ], check)
        })
      })
    })
    console.log('Does this appear before "Got books:"?')

    db.run('PRAGMA user_version = 4', check)
  }
  else {
    console.log('Patch level not required : 4')
  }

  do_patch(db, level, 4, 5, patches[4].forward)

  console.log('Finished serialising patches')
}

// patch the DB in sequence
db.serialize(() => {
  console.log('Creating database schema')

  // get the patch number first, then perform all patches required
  db.get('PRAGMA user_version',  (err, row) => {
    console.log('get user_schema:', err, row)
    console.log('Got patch level: ' + row.user_version)
    db.serialize(patch.bind(this, db, row.user_version, patches))
  })

  console.log('Database schema complete!')
})

// ----------------------------------------------------------------------------
// books

function allBooks(callback) {
  db.all('SELECT * from book', callback)
}

function getBook(isbn, callback) {
  db.get('SELECT * FROM book WHERE isbn = ?', [ isbn ], callback)
}

// Note: inserts return `this.lastID` to the callback if successful.
//
// See : https://github.com/mapbox/node-sqlite3/wiki/API#databaserunsql-param--callback
//
const sqlInsBook = 'INSERT INTO book(isbn, title, author, image, location, tags, note) VALUES(?, ?, ?, ?, ?, ?, ?)'
const sqlInsTag  = 'INSERT INTO tag(isbn, tag) VALUES(?, ?)'
const sqlInsStem = 'INSERT INTO stem(isbn, stem) VALUES(?, ?)'

function insBook(book, callback) {
  console.log('store.insBook()', { book })

  // firstly, validate all incoming params
  if ( !book.isbn ) {
    callback('ISBN must be provided')
    return
  }

  if ( !book.title ) {
    callback('Title must be provided')
    return
  }

  if ( !book.author ) {
    callback('Author must be provided')
    return
  }

  // remember all (plain) tags
  const tags = splitTags(book.tags)

  // firstly, list *all* words for all fields, and stem them
  const words = []
  words.push(...lodashWords(book.title))
  words.push(...lodashWords(book.author))
  words.push(...lodashWords(book.tags))
  console.log('words:', words)

  const noStopWords = stopword.removeStopwords(words)
  console.log('noStopWords:', noStopWords)

  // stem the lowercase word
  const allStems = noStopWords.map(word => stemr.stem(word.toLowerCase()))
  console.log('allStems:', allStems)

  // remove duplicates to uniquify the array
  const stems = [ ...new Set(allStems) ]
  console.log('stems:', stems)

  // set sticky serialize
  db.serialize()

  const params = [
    book.isbn,
    book.title,
    book.author,
    book.image,
    book.location,
    book.tags,
    book.note,
  ]
  db.run(sqlInsBook, params, (err) => {
    if (err) return callback(err)

    let completed = 0
    function done() {
      console.log('done()')
      completed++
      if (completed === stems.length) {
        db.parallelize()
        callback(null, this.lastID)
      }
    }

    // insert all tags
    for(var i = 0; i < tags.length; i++) {
      console.log('Inserting ' + tags[i])
      db.run(sqlInsTag, [ book.isbn, tags[i] ], done)
    }

    // insert all stems
    for(var i = 0; i < stems.length; i++) {
      console.log('Inserting ' + stems[i])
      db.run(sqlInsStem, [ book.isbn, stems[i] ], done)
    }
  })
}

// ----------------------------------------------------------------------------
// location

const sqlInsLocation = `INSERT INTO location(title, description, inserted, updated) VALUES(?, ?, ${now}, ${now})`
const sqlGetLocation = `SELECT * FROM location WHERE id = $?`
const sqlAllLocations = `SELECT * from location ORDER BY inserted`
const sqlUpdLocation = `UPDATE location SET title = ?, description = ?, updated = ${now} WHERE id = ?`

function allLocations(callback) {
  db.all(sqlAllLocations, callback)
}

function getLocation(id, callback) {
  db.get(sqlGetLocation, [ id ], callback)
}

function insLocation(location, callback) {
  console.log('store.insLocation()', { location })

  // firstly, validate all incoming params
  if ( !location.title ) {
    callback('Title must be provided')
    return
  }
  location.description = location.description || ''

  const params = [
    location.title,
    location.description,
  ]
  db.run(sqlInsLocation, params, function(err) {
    if (err) return callback(err)

    console.log('insLocation - this:', this)

    callback(null, this.lastID)
  })
}

function updLocation(location, callback) {
  console.log('store.updLocation()', { location })

  // firstly, validate all incoming params
  if ( !location.id ) {
    callback('ID must be provided')
    return
  }
  if ( !location.title ) {
    callback('Title must be provided')
    return
  }
  location.description = location.description || ''

  const params = [
    location.title,
    location.description,
    location.id,
  ]
  db.run(sqlUpdLocation, params, function(err, something, blah) {
    console.log('Update:', { this: this, err, something, blah })
    if (err) return callback(err)

    if (this.changes === 0) {
      // unknown ID
      callback(`Unknown ID (${location.id})`)
      return
    }

    console.log('updLocation - this:', this)

    callback(null, this.lastID)
  })
}

// ----------------------------------------------------------------------------
// query

function query(query, callback) {
  query = String(query).trim().toLowerCase()

  // first, get a list of words
  const words = lodashWords(query)
  // and now stems
  const allStems = words.map(word => stemr.stem(word))
  console.log('allStems:', allStems)
  // remove duplicates to uniquify the array
  const stems = [ ...new Set(allStems) ]
  console.log('stems:', stems)

  const result = {
    authors: [],
    titles: [],
    tags: [],
    stems: [],
  }

  let count = 0
  function done(err) {
    if (err) {
      return callback(err)
    }
    count = count - 1
    if ( count === 0 ) {
      db.parallelize()
      callback(null, result)
    }
  }

  // set sticky serialize
  db.serialize()

  // see if any authors match
  count = count + 1
  db.all("SELECT * FROM book WHERE lower(author) like '%' || ? || '%'", [ query ], (err, rows) => {
    console.log('Got author result:', err, rows)
    if (err) return callback(err)
    result.authors.push(...rows)
    done()
  })

  // see if any titles match
  count = count + 1
  db.all("SELECT * FROM book WHERE lower(title) like '%' || ? || '%'", [ query ], (err, rows) => {
    console.log('Got title result:', err, rows)
    if (err) return callback(err)
    result.titles.push(...rows)
    done()
  })

  // check for any tags
  count = count + 1
  db.all('SELECT * FROM book INNER JOIN tag ON tag.isbn = book.isbn WHERE lower(tag.tag) = ?', [ query ], (err, rows) => {
    if (err) return callback(err)
    result.tags.push(...rows)
    done()
  })

  // check on any stems
  for(let i = 0; i < stems.length; i++) {
    count = count + 1
    db.all('SELECT * FROM book INNER JOIN stem ON stem.isbn = book.isbn WHERE stem.stem = ?', [ stems[i] ], (err, rows) => {
      if (err) return callback(err)
      result.stems.push(...rows)
      done()
    })
  }
}

function close(callback) {
  db.close(callback)
}

// ----------------------------------------------------------------------------

module.exports = {
  // books
  allBooks,
  insBook,
  getBook,

  // locations
  allLocations,
  insLocation,
  getLocation,
  updLocation,

  // query
  query,

  // database
  db,
  close,

  // constants
  SQLITE_ERROR,
  SQLITE_INTERNAL,
  SQLITE_PERM,
  SQLITE_ABORT,
  SQLITE_BUSY,
  SQLITE_LOCKED,
  SQLITE_NOMEM,
  SQLITE_READONLY,
  SQLITE_INTERRUPT,
  SQLITE_IOERR,
  SQLITE_CORRUPT,
  SQLITE_NOTFOUND,
  SQLITE_FULL,
  SQLITE_CANTOPEN,
  SQLITE_PROTOCOL,
  SQLITE_EMPTY,
  SQLITE_SCHEMA,
  SQLITE_TOOBIG,
  SQLITE_CONSTRAINT,
  SQLITE_MISMATCH,
  SQLITE_MISUSE,
  SQLITE_NOLFS,
  SQLITE_AUTH,
  SQLITE_FORMAT,
  SQLITE_RANGE,
  SQLITE_NOTADB,
  SQLITE_NOTICE,
  SQLITE_WARNING,
  SQLITE_ROW,
  SQLITE_DONE,
}

// ----------------------------------------------------------------------------
