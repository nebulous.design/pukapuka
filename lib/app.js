// ----------------------------------------------------------------------------

// core
const path = require('path')

// npm
const express = require('express')
const compress = require('compression')
const favicon = require('serve-favicon')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const multer  = require('multer')
const cookieSession = require('cookie-session')
const bcrypt = require("bcrypt")
const RateLimit = require('express-rate-limit')
const cloudinary = require('cloudinary')

// local
const middleware = require('./middleware.js')
const store = require('./store.js')

// ----------------------------------------------------------------------------
// setup

const isProd = process.env.NODE_ENV === 'production'

// RateLimit some endpoints, such as app.post('/sign-in')
const signInLimiter = new RateLimit({
  windowMs   : 5 * 60 * 1000,   // 5 mins
  max        : 5,               // only allow 5 attempts
  delayAfter : 1,               // begin slowing down responses after the first request
  delayMs    : 5 * 1000,        // slow down subsequent responses by an additional 5s each request
  message    : "Too many requests, please try again in 5 minutes.\n"
})

// Google Books API Key
const googleBooksApiKey = process.env.GOOGLE_BOOKS_API_KEY

// Uploads
const upload = multer({
  dest : '/tmp/uploads',
  limits : {
    fileSize : 5 * 1024 * 1024, // 5MB
  },
})

// Cloudinary
cloudinary.config({
  cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
  api_key: process.env.CLOUDINARY_API_KEY,
  api_secret: process.env.CLOUDINARY_API_SECRET,
  secure: true,
})

// ----------------------------------------------------------------------------
// the app

var app = express()
app.set('case sensitive routing', true)
app.set('strict routing', true)
app.set('views', path.join(__dirname, '..', 'views'))
app.set('view engine', 'pug')
app.enable('trust proxy')

app.locals.title = process.env.TITLE || 'No Title'
if ( googleBooksApiKey ) {
  app.locals.googleBooksApiKey = googleBooksApiKey
}

// middleware

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(compress())
app.use(favicon(path.join(__dirname, '..', 'public', 'favicon.ico')))
app.use(express.static('public'))
app.use(morgan(isProd ? 'combined' : 'dev'))
app.use(upload.single('input'))

app.use(cookieSession({
  name   : 'session',
  keys   : [ process.env.SESSION_KEY ],
  maxAge : 24 * 60 * 60 * 1000, // 24 hours
}))

app.use((req, res, next) => {
  if ( req.session.user ) {
    res.locals.user = req.session.user
  }

  next()
})

// routes

app.get("/", (req, res) => {
  res.render('index', { active: 'index' })
})

app.get("/search", (req, res, next) => {
  // get the query, split it into words, stem the words
  const { query } = req.query

  // ask the store what this query matches - returns these lists:
  //
  // 1. authors
  // 2. titles
  // 3. tags
  // 4. stems
  //
  store.query(query, (err, result) => {
    if (err) return next(err)
    const locals = {
      active: 'search',
      query: query,
      result: result,
    }
    res.render('search', locals)
  })
})

app.get('/book/:isbn', (req, res, next) => {
  store.getBook(req.params.isbn, (err, book) => {
    if (err) return next(err)
    if (!book) {
      res.status(404)
    }
    res.render('book', {
      book,
      isbn: req.params.isbn,
      active: 'book',
    })
  })
})

app.get('/books', (req, res, next) => {
  store.allBooks((err, rows) => {
    if (err) return next(err)
    res.render('books', {
      books: rows,
      active: 'books',
    })
  })
})

// only add any admin routes if a PASSWORD env var was found
if ( process.env.PASSWORD ) {
  const hash = process.env.PASSWORD

  app.get(
    '/sign-in',
    middleware.redirectToAdminIfLoggedIn,
    (req, res) => {
      res.render('sign-in', {
        active: 'index',
        errs : {},
      })
    }
  )

  app.post(
    '/sign-in',
    middleware.redirectToAdminIfLoggedIn,
    signInLimiter,
    (req, res, next) => {
      const password = req.body.password

      // check the password is correct
      bcrypt.compare(password, hash, (err, ok) => {
        if (err) return next(err)

        // if the password is incorrect, just re-render the sign-in form (with the username again)
        if ( !ok ) {
          res.render('sign-in', {
            active: 'index',
            err: 'Error signing in',
            errs: {
              password : 'Incorrect password',
            },
          })
          return
        }

        // looks like the user signed in ok
        req.session.user = {
          ok   : true,
          msg  : 'Signed In',
          time : (new Date()).toISOString(),
        }
        res.redirect(302, '/admin/')
      })
    }
  )

  // --- api ---

  app.get(
    '/api/book/',
    middleware.apiCheckUser,
    middleware.loadAllBooks,
    (req, res) => {
      res.json(res.locals.books)
    }
  )

  app.get(
    '/api/location/',
    middleware.apiCheckUser,
    middleware.loadAllLocations,
    (req, res) => {
      res.json(res.locals.locations)
    }
  )

  app.post(
    '/api/location/new',
    middleware.apiCheckUser,
    (req, res) => {
      // figure out if there is a known user
      console.log('user:', req.session.user)
      console.log('body:', req.body)
      store.insLocation(req.body, (err, id) => {
        if (err) {
          console.warn(err)
          return res.status(500).json({ err: 'Internal Server Error' })
        }
        res.json({ id })
      })
    }
  )

  app.post(
    '/api/location/:id/',
    middleware.apiCheckUser,
    (req, res) => {
      // figure out if there is a known user
      console.log('user:', req.session.user)
      console.log('body:', req.body)
      const params = {
        id: req.params.id,
        title: req.body.title,
        description: req.body.description,
      }
      store.updLocation(params, (err, id) => {
        if (err) {
          console.warn(err)
          if (typeof err === 'string') {
            return res.json({ err })
          }
          return res.status(500).json({ err: 'Internal Server Error' })
        }
        res.json({ id })
      })
    }
  )

  // --- admin ---

  app.use('/admin', middleware.ensureUser)

  app.get('/admin', (req, res) => {
    res.redirect('/admin/')
  })

  app.get('/admin/book', (req, res) => {
    res.redirect('/admin/book/')
  })

  app.get('/admin/book/', (req, res, next) => {
    res.render('admin_book', { active: 'admin_book' })
  })

  app.post('/admin/book/new', (req, res, next) => {
    function renderErr(err) {
      res.render('admin', {
        form: req.body,
        err,
        active: 'admin',
      })
    }

    const book = {
      isbn: req.body.isbn,
      title: req.body.title,
      author: req.body.author,
      // image?
      location: req.body.location,
      tags: req.body.tags,
      note: req.body.note,
    }

    store.insBook(book, (err, id) => {
      if (err) {
        if (err.errno === store.SQLITE_CONSTRAINT) {
          renderErr(`Duplicate ISBN '${req.body.isbn}'. Already exists in database.`)
          return
        }
        renderErr('' + err)
        return
      }

      res.redirect('/admin/?msg=book-added')
    })
  })

  app.get('/admin/location', (req, res) => {
    res.redirect('/admin/location/')
  })

  app.get('/admin/location/', (req, res, next) => {
    res.render('admin_location', { active: 'admin_location' })
  })

  app.post('/admin/image/new', (req, res, next) => {
    // if there is no file, tell the user they need to upload one
    if ( !req.file ) {
        console.warn('No req.file')
        res.setHeader('Content-Type', 'text/plain')
        return res.status(500).send('Provide an image.')
    }

    // Example:
    //
    // req.file: {
    //   fieldname: 'input',
    //   originalname: '20180612_210008.jpg',
    //   encoding: '7bit',
    //   mimetype: 'image/jpeg',
    //   destination: '/tmp/uploads',
    //   filename: 'f6567302f51a88009c9f706d148b2b92',
    //   path: '/tmp/uploads/f6567302f51a88009c9f706d148b2b92',
    //   size: 2991273
    // }

    const encodedFilename = encodeURIComponent(req.file.originalname)

    // upload this file
    cloudinary.v2.uploader.upload(req.file.path, (err, result) => {
      if (err) return next(err)

      const url = cloudinary.url(result.public_id, { secure: true, width: 640, crop: 'fit' })

      // send JSON for now
      res.json({ file : req.file, result : result })
    })
  })

  app.get(
    '/admin/',
    middleware.loadAllLocations,
    (req, res) => {
      res.render('admin', {
        msg: req.query.msg,
        form: {},
        active: 'admin',
      })
    }
  )
}

app.get('/sign-out', (req, res) => {
  // destroy the session
  req.session = null

  // redirect back to homepage
  return res.redirect(302, '/')
})

// ----------------------------------------------------------------------------

module.exports = app

// ----------------------------------------------------------------------------
