var app = new Vue({
  el: '#app',
  data: {
    books: [],
  },
  methods: {
    loadBooks: () => {
      axios.get('/api/book/')
        .then((res) => {
          console.log('Got:', res.data)
          console.log('Got:', typeof res.data)
          app.books = res.data
        })
        .catch((err) => {
          console.warn('Error loading books:', err)
          app.books = []
        })
      ;
    },
  },
})

app.loadBooks()
