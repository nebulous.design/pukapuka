var app = new Vue({
  el: '#app',
  data: {
    id: 0,
    title: '',
    description: '',
    err: '',
    show: {
      form: false,
    },
    loading: false,
    locations: [],
  },
  computed: {
    isSubmitDisabled: (app) => {
      console.log('computed.isSubmitDisabled()')
      if (app.loading) {
        return true
      }
      if (app.title.trim() === '') {
        return true
      }
      return false
    },
    areInputsDisabled: (app) => {
      console.log('computed.areInputsDisabled()')
      return app.loading
    },
    submitIcon: (app) => {
      return app.loading ? 'fa-spinner fa-spin' : 'fa-plus-circle'
    },
  },
  methods: {
    loadLocations: () => {
      axios.get('/api/location/')
        .then((res) => {
          console.log('Got:', res.data)
          console.log('Got:', typeof res.data)
          app.locations = res.data
        })
        .catch((err) => {
          console.warn('Error loading locations:', err)
          app.locations = []
        })
      ;
    },
    onClickNewLocation: () => {
      app.err = ''
      app.id = 0
      app.title = ''
      app.description = ''
      app.show.form = true
      console.log('refs:', app.$refs)
      Vue.nextTick(() => {
        // DOM updated
        app.$refs.title.focus()
      })
    },
    onClickEdit: (location) => {
      console.log('Editing:', { location })
      app.err = ''
      app.id = location.id
      app.title = location.title
      app.description = location.description
      app.show.form = true
      Vue.nextTick(() => {
        // DOM updated
        app.$refs.title.focus()
      })
    },
    onClickDelete: (location) => {
      // NOTE: before doing this, add a `/admin/location/:id/` page so the user can see details of the location,
      // including books so that we can re-use this information here.
      console.log('ToDo')
    },
    onClickModalBackground: () => {
      app.show.form = false
    },
    onClickClose: () => {
      app.show.form = false
    },
    onClickCancel: () => {
      app.show.form = false
    },
    onSubmit: (ev) => {
      app.loading = true

      let url = '/api/location/new'
      if (app.id) {
        url = '/api/location/' + app.id + '/'
      }

      axios.post(url, {
        title: app.title,
        description: app.description,
      })
        .then(function (res) {
          const data = res.data
          console.log('Got:', data)
          if ( data.err ) {
            app.err = data.err
            app.loading = false
            return
          }
          app.loadLocations()
          app.loading = false
          app.id = 0
          app.title = ''
          app.description = ''
          app.show.form = false
        })
        .catch(function(err) {
          console.warn('Error saving location:', err)
          app.loading = false
          app.err = '' + err
        })
      ;
    },
  },
})

app.loadLocations()
