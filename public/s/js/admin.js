// ----------------------------------------------------------------------------
// helpers

function val(id) {
  return document.getElementById(id).value
}

function getBookInfo(isbn, callback) {
  console.log('getBookInfo - isbn:', isbn)

  const googleBooksApiKey = val('googleBooksApiKey')

  axios.get('https://www.googleapis.com/books/v1/volumes', {
    params: {
      q: 'isbn:' + isbn,
      key: googleBooksApiKey,
    },
  })
    .then(function (response) {
      console.log('Google Books API Data:', response.data)
      callback(null, response.data)
    })
    .catch(function(err) {
      callback(err)
    })
  ;
}

// ----------------------------------------------------------------------------
// app

var app = new Vue({
  el: '#google-books-api',
  data: {
    isbn: '',
    searchLoading: false,
    newBook: {
      isbn: val('isbn'),
      title: val('title'),
      author: val('author'),
      location: val('location'),
      tags: val('tags'),
      note: val('note'),
    },
    googleBooks: {
      err: '',
      data: {},
    },
  },
  methods: {
    onGoogleBooksApiSearch: (ev) => {
      ev.preventDefault()

      app.searchLoading = true
      app.googleBooks.data = {}
      app.googleBooks.err = ''

      getBookInfo(app.isbn, function(err, data) {
        if (err) {
          app.searchLoading = false
          return console.warn(err)
        }
        app.searchLoading = false
        if ( data.totalItems === 0 ) {
          app.googleBooks.err = 'Unknown ISBN'
        }
        else {
          app.googleBooks.err = ''
          app.googleBooks.data = data
        }
      })
    },
    onCopyToNewBookForm: (ev) => {
      ev.preventDefault()

      app.newBook.isbn = app.googleBooks.data.items[0].volumeInfo.industryIdentifiers[0].identifier
      app.newBook.title = app.googleBooks.data.items[0].volumeInfo.title
      app.newBook.author = app.googleBooks.data.items[0].volumeInfo.authors[0]
    },
  },
})

// ----------------------------------------------------------------------------
