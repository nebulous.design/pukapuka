# Pukapuka #

This is a simple book management system. It is open source (MIT License) and is available for anyone to run for
anything they choose.

## About Glitch ##

Glitch is a free platform for hosting small websites such as Pukapuka.

To run Pukapuka on Glitch there are a few things you need to do:

1. ToDo
1. ToDo
1. ToDo
1. ToDo

Find out more [about Glitch](https://glitch.com/about).


## Made By ##

|               | Website                                     | Twitter                                         |
|:-------------:|:-------------------------------------------:|:-----------------------------------------------:|
| Project | [Pukapuka](https://gitlab.com/nebulous.design/pukapuka/) | - |
| Company | [Nebulous Design](https://nebulous.design/) | [@NebulousDesign](https://twitter.com/NebulousDesign) |
| Author | [Andrew Chilton](https://chilts.org/) | [@andychilton](https://twitter.com/andychilton) |

(Ends)
