/* ------------------------------------------------------------------------- */

@tailwind preflight;
@tailwind components;

/* ------------------------------------------------------------------------- */
/* Elements */

body {
    @apply font-open-sans text-grey-darker;
}

h1, h2, h3, h4, h5, h6 {
    @apply font-gentium-book-basic mb-4;
}

h1 { @apply text-4xl; }
h2 { @apply text-3xl; }
h3 { @apply text-2xl; }
h4 { @apply text-xl; }
h5 { @apply text-lg; }
h6 { @apply text-base; }

.logo {
    @apply font-gentium-book-basic;
}

p {
    @apply mb-6;
}

a {
    @apply text-blue-dark no-underline;
}

a:hover {
    @apply text-blue-darker;
}

/* Tables */

table {
    @apply w-full leading-normal;
    border-collapse: collapse;
    border-spacing: 0;
}

table thead {
    @apply border-b border-grey;
}

table tfoot {
    @apply border-t border-grey;
}

th, td {
    @apply text-left p-2;
}

tbody tr:nth-child(even) {
    @apply bg-grey-lightest;
}

tbody tr:nth-child(odd) {
    @apply bg-grey-lighter;
}

dt {
   @apply font-bold;
}

dd {
   @apply pl-6;
}

/* ------------------------------------------------------------------------- */
/* Buttons */

/* Choose `.btn` OR `.btn-icon`, then any `.btn-<color>` below. */

.btn {
    @apply font-bold py-2 px-4 mx-1 rounded;
}

.btn-icon {
    @apply font-bold text-sm p-2 mx-1 rounded;
}

.btn:disabled {
    @apply cursor-not-allowed opacity-50;
}

.btn-primary {
    @apply bg-blue text-white;
}
.btn-primary:hover {
    @apply bg-blue-dark;
}

.btn-primary-outline {
    @apply text-blue bg-white border border-blue;
}
.btn-primary-outline:hover {
    @apply text-white bg-blue-dark border-transparent;
}

/* ------------------------------------------------------------------------- */
/* Components */

nav a {
    @apply text-white;
}

nav a:hover {
    @apply text-blue-lighter;
}

footer {
    @apply text-white;
}

footer a {
    @apply text-blue-lighter font-bold;
}

footer a:hover {
    @apply text-blue-light;
}

/* Modals */

.modal {
    @apply fixed pin flex justify-center z-50 overflow-auto;
    background-color: rgba(0, 0, 0, 0.4);
}

.modal-body {
    @apply fixed m-auto p-8 bg-white;
    margin-top: 150px;
}

/* Specific Components */

#search-front-page {
    height: 50vh;
    min-height: 50vh;
}

#search-results {
}

/* ------------------------------------------------------------------------- */

@tailwind utilities;
@tailwind screens;

/* ------------------------------------------------------------------------- */
