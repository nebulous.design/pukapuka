dump:
	sqlite3 .data/sqlite.db .dump

dump-file:
	sqlite3 .data/sqlite.db .dump > dump-$$(date +'%Y%m%d-%H%M%S').sql
